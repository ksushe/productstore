module.exports = {
    //Generate random email address
    //Example: NotReal_057602943343303625@mail.com
    generateRandomEmailAddress: function () {
        let emailAddress = "NotReal_" + Math.random().toString().replace('0.', '') + "@mail.com"
        return emailAddress;
    },

    //Generate random string
    //Example: d7913n4vixuutisf9eopza
    generateRandomString: function () {
        return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    },

    //Generate random number
    //Example: 057602943343303625
    generateRandomNumber: function () {
        return Math.random().toString().replace('0.', '');
    }
}