import Base_PO from './Base_PO'

class Monitors_PO extends Base_PO {
    get firstMonitor() {
        return $("//div[@id='tbodyid']/div[1]/div[@class='card h-100']/a[@href='prod.html?idp_=10']");
    }
    get pricePresented() {
        return $("//div[@id='tbodyid']/div[1]/div[@class='card h-100']");
    }

    monitorSelect() {
        //Getting the price and clicking on the first monitor  
        let firstPrice = this.pricePresented.getText();
        return firstPrice;
        //this.firstMonitor.click();
    }
}

export default new Monitors_PO();