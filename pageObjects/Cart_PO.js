import Base_PO from './Base_PO'
let config = require('../config/main-config');
let dataGenerators = require('../utils/dataGenerators');


class Cart_PO extends Base_PO {
    get cartNavBar() {
        return $("#cartur");
    }
    get placeOrderButton() {
        return $("//div[@id='page-wrapper']//button[@type='button']")
    }
    get priceToPay() {
        return $('//*[@id="tbodyid"]/tr[1]');
    }
    get priceToPay2() {
        return $('//*[@id="tbodyid"]/tr[2]');
    }
    get name() {
        return $("#name");
    }
    get country() {
        return $("#country");
    }
    get city() {
        return $("#city");
    }
    get creditCard() {
        return $("#card");
    }
    get month() {
        return $("#month");
    }
    get year() {
        return $("#year");
    }
    get purchaseButton() {
        return $("//div[@id='orderModal']/div[@role='document']//div[@class='modal-footer']/button[2]")
    }
    get successfulContactHeader() {
        return $("//body/div[10]/h2[.='Thank you for your purchase!']")
    }
    get amount() {
        return $("//body/div[10]/p");
    }
    get deleteOneItem() {
        return $("/html//tbody[@id='tbodyid']/tr[1]/td[4]/a[@href='#']");
    }

    cartMenu() {
        //Movement to the cart menu
        this.cartNavBar.click();
        browser.pause(5000);
    }

    cartPrise() {

        //getting the price of the product added to the cart
        //let lastPrice = this.priceToPay.getValue();
        var sum = 0;
        for (let i = 1; i < 3; i++) {
            if ($(`//*[@id="tbodyid"]/tr[${i}]/td[3]`).isExisting() === true) {

                sum += parseInt($(`//*[@id="tbodyid"]/tr[${i}]/td[3]`).getText());
            } else {
                break;
            }
        }
        console.log(sum);
        return sum;
    }

    //Fill in the registration fields and receive a message about successful registration
    successfulPurchase() {
        this.cartPrise();
        //Clicking on the "Place Order" button
        this.placeOrderButton.click();
        //expect fields to be displayed
        this.name.waitForDisplayed(5000);
        //fill in the fields with data (export from main-config.js file)
        this.name.setValue(config.name);
        this.country.setValue(config.country);
        this.city.setValue(config.city);
        //fill in the field with random generate data (export from dataGenerators.js)
        this.creditCard.setValue(dataGenerators.generateRandomNumber());

        //Clicking on the "Purchase" button
        this.purchaseButton.click();

        //Receiving a successful registration message 'Thank you for your purchase!'
        expect(this.successfulContactHeader.getText()).to.contains('Thank you for your purchase!');
        expect(this.amount.getText()).to.contains(this.cartPrise().toString());
    }

    deleteItem() {
        //Movement to the cart menu
        this.cartNavBar.click();
        browser.pause(5000);

        this.deleteOneItem.click();
        browser.pause(5000);
        expect(this.priceToPay.isExisting()).to.equal(false);
    }

}
export default new Cart_PO();


// //*[@id="tbodyid"]/tr[1]/td[3]

// tips() {
//     let sum;
//     for (let i = 1; i < 10; i++) {
//         if ($(`//*[@id="tbodyid"]/tr[${i}]/td[3]`).getValue()) {
//             sum = sum + parseInt($(`//*[@id="tbodyid"]/tr[${i}]/td[3]`).getValue());
//             break;
//         } //else {
//         //     sum = parseInt($(`//*[@id="tbodyid"]/tr[${i}]/td[3]`).getValue())
//         // }
//     }
//     //return sum;
// }
// tips() {
//     let sum;
//     for (let i = 1; i < 10; i++) {
//         while ($(`//*[@id="tbodyid"]/tr[${i}]/td[3]`).isExisting()) {
//             sum = sum + parseInt($(`//*[@id="tbodyid"]/tr[${i}]/td[3]`).getValue());
//         }
//     }

//     //return sum;
// }