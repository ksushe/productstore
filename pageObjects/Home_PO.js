import Base_PO from './Base_PO'
let config = require('../config/main-config');
let dataGenerators = require('../utils/dataGenerators');

class Home_PO extends Base_PO {
    //Open browser window (export base url from main-config.js file)
    open() {
        super.open(config.baseUrl);
    }

    get homeNavBar() {
        return $("//div[@id='navbarExample']/ul//a[@href='index.html']");
    }
    get signUpNavBar() {
        return $("#signin2");
    }
    get logInNavBar() {
        return $("#login2");
    }
    get welcomeNavBar() {
        return $("#nameofuser");
    }
    get monitorsCategory() {
        return $("/html//div[@id='contcont']//div[@class='list-group']/a[4]")
    }
    get firstPhone() {
        return $("//h4/a[@href='prod.html?idp_=1']");
    }
    get pricePresented() {
        return $("//div[@id='tbodyid']/div[1]/div[@class='card h-100']");
    }
    get userNmae() {
        return $("#sign-username");
    }
    get userPassword() {
        return $("#sign-password");
    }
    get signUpButton() {
        return $('//*[@id="signInModal"]/div/div/div[3]/button[2]');
    }
    get loginUserName() {
        return $("#loginusername");
    }
    get loginPassword() {
        return $("#loginpassword");
    }
    get loginButton() {
        return $("//div[@id='logInModal']/div[@role='document']//div[@class='modal-footer']/button[2]");
    }

    phoneSelect() {
        //Getting the price and clicking on the first product 
        var firstPrice = this.pricePresented.getText();
        return firstPrice;
        //this.firstPhone.click();

    }
    registration() {
        //Waiting for a  sign up button to display and clicking on it
        browser.waitAndClick(this.signUpNavBar);
        //expect fields to be displayed
        this.userNmae.waitForDisplayed(5000);
        //Filling in fields with random data 
        this.userNmae.setValue(dataGenerators.generateRandomString());
        this.userPassword.setValue(dataGenerators.generateRandomString());
        this.signUpButton.click();
        browser.pause(3000);

        expect(browser.getAlertText()).to.contains('Sign up successful.');
        browser.pause(3000);
        browser.acceptAlert();
    }

    login() {
        //Waiting for a  log in button to display and clicking on it
        browser.waitAndClick(this.logInNavBar);
        //expect fields to be displayed
        this.loginUserName.waitForDisplayed(5000);
        //Filling in fields with base data
        this.loginUserName.setValue(config.name);
        this.loginPassword.setValue(config.password);
        this.loginButton.click();
        browser.pause(3000);

        expect(this.welcomeNavBar.isExisting()).to.equal(true);
    }

}
export default new Home_PO();