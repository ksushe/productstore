//Export basic data from a file main-config.js
let config = require('../config/main-config');

export default class Base_PO {
    //Open a browser window at the specified url and specify the window size
    open(path) {
        //specify the base url of the page 
        browser.url(path);
        // specify the width and height of the window (from main-config.js file) 
        browser.setWindowSize(config.windowWidth, config.windowHeight);
    }

    //Set a pause in the test
    waitUsingFixedTimeout(time) {
        console.log("Pausing for: " + time + " seconds.");
        browser.pause(time);
    }
}