import Base_PO from './Base_PO'

class Product_PO extends Base_PO {

    get addToCartButton() {
        return $("//div[@id='tbodyid']//a[@href='#']");
    }

    addToCart() {
        //Waiting for a  "Add to cart" button to display and clicking on it
        browser.waitAndClick(this.addToCartButton);
        browser.pause(3000);
        //checking the successful addition of the product to the cart
        expect(browser.getAlertText()).to.contains('Product added');
        //confirmation of action on the page
        browser.acceptAlert();
        browser.pause(3000);
    }

}
export default new Product_PO();