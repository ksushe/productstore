## WebdriverUniversity
Testing WebdriverUniversity website registration page.

## Table of contents
* Technologies
* General info
* Launch

## Technologies

*
*
*
1. Install Node.js(node - v in terminal to see your version).
2. json => type npm init in terminal.
3. WebdriverIO .

## General info

The document contains such working folders:
* config
* pageObjects
* test
* utils

The config folder contains the main-config.js file  with basic data for testing.
The pageObjects folder contains files with basic functions (Base_PO.js) and objects (ContactUs_Page.js).
The test folder contains a test file.
The utils folder contains dataGenerators.js file for generating random strings and mailing addresses.


## Launch

Before starting the test, open the  main-config.js file in the config folder. Enter test data (browser, test page address, test name, etc.). Save the changes.
Target on the test file in terminal:

```
$ npm test-- --spec=test/contactUsTestPOM.js 
```



