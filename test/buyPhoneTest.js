import Home_PO from '../pageObjects/Home_PO';
import Product_PO from '../pageObjects/Product_PO';
import Cart_PO from '../pageObjects/Cart_PO';
import Monitors_PO from '../pageObjects/Monitors_PO';

describe('Product purchase testing on Product Store', () => {
    beforeEach(function () {
        Home_PO.open();
    });

    it('Phone purchase testing on Product Store', () => {
        //Selection the first phone 
        //Home_PO.open();
        let cost = Home_PO.phoneSelect();
        Home_PO.firstPhone.click();

        //Adding the first phone to the cart
        Product_PO.addToCart();

        //Movement to the cart menu
        Cart_PO.cartMenu();

        //Checking the compliance of the initial price with the final price
        expect(cost).to.contains(Cart_PO.cartPrise().toString());

        //Filling out an order form and receiving a successful registration message
        Cart_PO.successfulPurchase();
        browser.pause(3000);

    })

    it.skip('Several product purchase testing on Product Store', () => {
        //Selection the first phone 
        //Home_PO.open();
        let cost = Home_PO.phoneSelect();
        Home_PO.firstPhone.click();

        //Adding item  to the cart
        Product_PO.addToCart();

        //Returning to the home page
        Home_PO.homeNavBar.click();
        browser.pause(3000);

        //Click on the monitors category
        Home_PO.monitorsCategory.click();

        //Selection the first monitor 
        let cost2 = Monitors_PO.monitorSelect();
        Monitors_PO.firstMonitor.click();

        //Adding item to the cart
        Product_PO.addToCart();

        //Movement to the cart menu
        Cart_PO.cartMenu();
        //expect(cost).to.contains(Cart_PO.cartPrise().toString());
        //expect(cost2).to.contains(Cart_PO.cartPrise().toString());

        //Filling out an order form and receiving a successful registration message
        Cart_PO.successfulPurchase();
        browser.pause(3000);

    })

    it('Check the removal of goods from the cart', () => {
        //Selection the first phone 
        //Home_PO.open();
        Home_PO.phoneSelect();
        Home_PO.firstPhone.click();

        //Adding the first phone to the cart
        Product_PO.addToCart();

        //Delete item from the cart
        Cart_PO.deleteItem();
    })

    it('Registration test on the site', () => {
        //Home_PO.open();
        Home_PO.registration();
    })

    it('Login test', () => {
        //Home_PO.open();
        Home_PO.login();
    })

});